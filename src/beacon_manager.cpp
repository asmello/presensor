#include "beacon_manager.hpp"

#include <chrono>

void BeaconManager::record(Beacon b)
{
    b.data.first_detection = (_record.count(b.id) == 0);
    b.data.last_time = std::chrono::system_clock::now();
    _record[b.id] = b.data;
    _evth->notifyDetection(b);
}

void BeaconManager::update()
{
    auto now = std::chrono::system_clock::now();
    for (auto it = _record.begin(); it != _record.end();)
    {
        if (now - it->second.last_time > std::chrono::seconds(5)) {
            _evth->notifyLoss(it->first);
            _record.erase(it++);
        } else {
            ++it;
        }
    }
    _evth->flush();
}
