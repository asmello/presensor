#include "event_handler.hpp"

#include <stdexcept>
#include <iostream>
#include <fstream>
#include <chrono>
#include <ctime>

Json::Value EventHandler::queryCredentials()
{
    unsigned int pin;
    bool pin_valid = false;

    std::cout << "Renewing credentials..." << std::endl;

    while (!pin_valid) {
        std::cout << "[*] Enter authentication PIN: " << std::flush;
        std::cin >> pin;
        if (std::cin.good()) {
            pin_valid = true;
        } else {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cerr << "[x] Could not parse PIN. Try again." << std::endl;
        }
    }

    std::string query = "?pin=" + std::to_string(pin);

    Json::Value cred;
    try {
        _cinst.get(_base_url + "/api/auth" + query);
        long code = _cinst.getResponseCode();
        if (code != Curl::HTTP_OK) {
            std::cerr << "[x] Request failed with code " << code << std::endl;
            std::cerr << "[x] Server message: " << _cinst << std::endl;
            return Json::Value();
        }
    } catch (const Curl::curl_error& e) {
        std::cerr << "[x] cURL: " << e.what() << std::endl;
        return Json::Value();
    }

    try {
        _cinst >> cred;
    } catch (const std::exception& e) {
        std::cerr << "[x] Could not parse server response." << std::endl;
        return Json::Value();
    }

    if (!cred.isMember("user") || !cred["user"].isString()
        || !cred.isMember("pass") || !cred["pass"].isString()) {
        std::cerr << "[x] Got strange response: " << cred << std::endl;
        return Json::Value();
    }

    std::cout << "[i] Got credentials: " << cred << std::endl;
    return cred;
}

EventHandler::EventHandler(const std::string & hostname, short unsigned port)
: _cinst()
{
    if (hostname.empty()) {
        throw new std::runtime_error("[x] Got an empty hostname.");
    }

    if (port == 80) {
        _base_url = "http://" + hostname;
    } else {
        _base_url = "http://" + hostname + ":" + std::to_string(port);
    }

    Json::Value cred;
    std::fstream fcred("bscan.conf", std::ios_base::out | std::ios_base::in);

    if (fcred.is_open()) {
        try {
            std::cout << "[i] Reading credentials from file: bscan.conf"
                      << std::endl;
            fcred >> cred;
        } catch (std::exception& e) {
            std::cerr << "[x] Could not parse stored credentials." << std::endl;
            cred = queryCredentials();
            if (cred.isNull()) {
                throw std::runtime_error("[x] Failed to acquire credentials.");
            }
        }
        fcred.close();
    } else {
        fcred.clear();

        cred = queryCredentials();
        if (cred.isNull()) {
            throw std::runtime_error("[x] Failed to acquire credentials.");
        }

        fcred.open("bscan.conf", std::ios_base::out);
        if (!fcred.is_open()) {
            std::cerr << "[!] Cannot save credentials, "
                "will need to renew on restart." << std::endl;
        } else {
            fcred << cred;
            fcred.close();
        }
    }

    _events["user"] = cred["user"];
    _events["pass"] = cred["pass"];
    _events["alerts"] = Json::arrayValue;
}

void EventHandler::notifyDetection(Beacon b)
{
    // Generate timestamp
    char str_time[21];
    std::time_t c_time = std::chrono::system_clock::to_time_t(b.data.last_time);
    strftime(str_time, 21, "%FT%TZ", std::gmtime(&c_time));

    // Generate a new json entry for the beacon
    Json::Value jentry;
    if (b.data.first_detection) {
        jentry["kind"] = "in";
    } else {
        jentry["kind"] = "update";
    }
    jentry["uuid"] = b.id.uuid;
    jentry["major"] = b.id.major;
    jentry["minor"] = b.id.minor;
    jentry["distance"] = b.data.distance();
    jentry["time"] = str_time;

    _events["alerts"].append(jentry); // Add it to event list
}

void EventHandler::notifyLoss(Beacon::ID id)
{
    Json::Value jentry;
    jentry["kind"] = "out";

    // Generate timestamp
    char str_time[21];
    auto now = std::chrono::system_clock::now();
    std::time_t now_c = std::chrono::system_clock::to_time_t(now);
    strftime(str_time, 21, "%FT%TZ", std::gmtime(&now_c));

    jentry["time"] = str_time;
    jentry["uuid"] = id.uuid;
    jentry["major"] = id.major;
    jentry["minor"] = id.minor;

    _events["alerts"].append(jentry);
}

void EventHandler::flush()
{
    if (_events["alerts"].empty()) return;

    try {
        const std::string eurl = _base_url + "/api/alert";
        std::cout << "[i] Sending alerts to " << eurl << std::endl;
        _cinst.post(eurl, "application/json", _events.toStyledString());

        std::cout << "[i] Message: " << _events << std::endl;

        long code = _cinst.getResponseCode();
        if (code != Curl::HTTP_OK) {
            std::cerr << "[x] Request failed with code " << code << std::endl;
            std::cerr << "[x] Server message: " << _cinst << std::endl;
        }

    } catch (const Curl::curl_error & e) {
        std::cerr << "[x] cURL: " << e.what() << std::endl;
    } catch (const std::exception & e) {
        std::cerr << "[x] Error: " << e.what() << std::endl;
    }

    _events["alerts"].clear();
}
