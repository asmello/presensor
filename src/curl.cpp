#include "curl.hpp"
#include "meta.hpp"

#include <cstdio>

int Curl::num_instances = 0;
const char * Curl::user_agent = "Presensor/" VERSION;

std::ostream& operator<<(std::ostream& output, const Curl& o)
{
    output << o._rcvss.rdbuf();
    return output;
}

std::istream& operator>>(std::istream& input, Curl& o)
{
    input >> o._sndss.rdbuf();
    return input;
}

Curl::Curl()
{
    CURLcode r;
    if (!Curl::num_instances == 0) {
        r = curl_global_init(CURL_GLOBAL_ALL);
        if (r != CURLE_OK) {
            throw std::runtime_error(curl_easy_strerror(r));
        }
    }
    _handle = curl_easy_init();
    if (_handle == NULL) {
        throw std::runtime_error("easy init failed");
    }
    curl_easy_setopt(_handle, CURLOPT_WRITEFUNCTION, Curl::write_data);
    curl_easy_setopt(_handle, CURLOPT_WRITEDATA, static_cast<void *>(&_rcvss));
    curl_easy_setopt(_handle, CURLOPT_READFUNCTION, Curl::read_data);
    curl_easy_setopt(_handle, CURLOPT_READDATA, static_cast<void *>(&_sndss));
    curl_easy_setopt(_handle, CURLOPT_USERAGENT, Curl::user_agent);
    Curl::num_instances++;
}

Curl::~Curl()
{
    curl_easy_cleanup(_handle);
    Curl::num_instances--;
    if (Curl::num_instances == 0) {
        curl_global_cleanup();
    }
}

size_t Curl::write_data(void *buf, size_t size, size_t nmemb, void *userp)
{
    const size_t bytes = size * nmemb;
    std::ostream* const ostr = static_cast<std::ostream*>(userp);
    ostr->write(static_cast<const char *>(buf),
                static_cast<std::streamsize>(bytes));
    return bytes;
}

size_t Curl::read_data(void *buf, size_t size, size_t nitems, void *userp)
{
    const size_t bytes = size * nitems;
    std::istream* const istr = static_cast<std::istream*>(userp);
    istr->read(static_cast<char *>(buf),
               static_cast<std::streamsize>(bytes));
    return istr->gcount();
}

void Curl::reset()
{
    curl_easy_reset(_handle);
}

void Curl::get(const std::string& addr)
{
    CURLcode r;

    r = curl_easy_setopt(_handle, CURLOPT_URL, addr.c_str());
    if (r != CURLE_OK) {
        throw curl_error(curl_easy_strerror(r));
    }

    r = curl_easy_setopt(_handle, CURLOPT_HTTPGET, 1L);
    if (r != CURLE_OK) {
        throw curl_error(curl_easy_strerror(r));
    }

    r = curl_easy_perform(_handle);

    if (r != CURLE_OK) {
        throw curl_error(curl_easy_strerror(r));
    }
}

void Curl::post(
    const std::string& addr, const std::string& mime, const std::string& msg
) {
    struct curl_slist *headers = NULL;

    std::string ctype = "Content-Type: " + mime;

    headers = curl_slist_append(headers, ctype.c_str());
    if (headers == NULL) {
        throw curl_error("post header creation failed");
    }

    CURLcode r;

    r = curl_easy_setopt(_handle, CURLOPT_URL, addr.c_str());
    if (r != CURLE_OK) {
        throw curl_error(curl_easy_strerror(r));
    }

    r = curl_easy_setopt(_handle, CURLOPT_POSTFIELDSIZE, msg.size());
    if (r != CURLE_OK) {
        throw curl_error(curl_easy_strerror(r));
    }

    r = curl_easy_setopt(_handle, CURLOPT_POSTFIELDS, msg.c_str());
    if (r != CURLE_OK) {
        throw curl_error(curl_easy_strerror(r));
    }

    r = curl_easy_setopt(_handle, CURLOPT_HTTPHEADER, headers);
    if (r != CURLE_OK) {
        throw curl_error(curl_easy_strerror(r));
    }

    r = curl_easy_perform(_handle);

    curl_slist_free_all(headers);

    if (r != CURLE_OK) {
        throw curl_error(curl_easy_strerror(r));
    }
}

void Curl::post(
    const std::string& addr, const std::string& mime, const std::string& msg,
    const size_t timeout
) {
    struct curl_slist *headers = NULL;

    std::string ctype = "Content-Type: " + mime;

    headers = curl_slist_append(headers, ctype.c_str());
    if (headers == NULL) {
        throw curl_error("post header creation failed");
    }

    CURLcode r;

    r = curl_easy_setopt(_handle, CURLOPT_URL, addr.c_str());
    if (r != CURLE_OK) {
        throw curl_error(curl_easy_strerror(r));
    }

    r = curl_easy_setopt(_handle, CURLOPT_POSTFIELDSIZE, msg.size());
    if (r != CURLE_OK) {
        throw curl_error(curl_easy_strerror(r));
    }

    r = curl_easy_setopt(_handle, CURLOPT_COPYPOSTFIELDS, msg.c_str());
    if (r != CURLE_OK) {
        throw curl_error(curl_easy_strerror(r));
    }

    r = curl_easy_setopt(_handle, CURLOPT_HTTPHEADER, headers);
    if (r != CURLE_OK) {
        throw curl_error(curl_easy_strerror(r));
    }

    r = curl_easy_setopt(_handle, CURLOPT_TIMEOUT, timeout);
    if (r != CURLE_OK) {
        throw curl_error(curl_easy_strerror(r));
    }

    r = curl_easy_perform(_handle);

    curl_slist_free_all(headers);

    if (r != CURLE_OK) {
        throw curl_error(curl_easy_strerror(r));
    }
}

long Curl::getResponseCode()
{
    long code;
    curl_easy_getinfo(_handle, CURLINFO_RESPONSE_CODE, &code);
    return code;
}

void Curl::setCredentials(const std::string& usr, const std::string& pwd)
{
    char *esc_usr = curl_easy_escape(_handle, usr.c_str(), usr.length());
    char *esc_pwd = curl_easy_escape(_handle, pwd.c_str(), pwd.length());
    const std::string fmt = std::string(esc_usr) + ":" + esc_pwd;
    curl_easy_setopt(_handle, CURLOPT_USERPWD, fmt.c_str());
    curl_free(esc_usr);
    curl_free(esc_pwd);
}
