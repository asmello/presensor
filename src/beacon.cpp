#include "beacon.hpp"

#include <cmath>

// Estimates the distance (in meters) from device calibration constant
// (transmitter power at 1m) and the measured power (rssi). Adapted from
// http://stackoverflow.com/questions/20416218
double Beacon::Data::distance()
{
    if (rssi == 0) return -1.0;

    double ratio = rssi*1.0/tx;

    if (ratio < 1.0) {
        return std::pow(ratio, 10);
    } else {
        return (0.89976) * std::pow(ratio, 7.7095) + 0.111;
    }
}
