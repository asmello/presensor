#include "beacon_scanner.hpp"
#include "event_handler.hpp"

#include <OptionParser.h>

#include <stdexcept>
#include <iostream>
#include <memory>

int main(int argc, const char *argv[])
{
    optparse::OptionParser parser = optparse::OptionParser()
        .usage("usage: %prog [options] hostname")
        .description("A command-line bluetooth beacon sensor "
                     "and client for the PreSense system.");

    parser.add_option("-p", "--port").dest("port").type("int")
          .help("remote server port").metavar("port");
    parser.set_defaults("port", "80");

    optparse::Values options = parser.parse_args(argc, argv);
    std::vector<std::string> args = parser.args();

    if (args.size() != 1) {
        parser.error("incorrect number of arguments");
    }

    std::shared_ptr<EventHandler> evth;
    try {
         evth = std::make_shared<EventHandler>(args[0], options.get("port"));
    } catch (const std::runtime_error & e) {
        std::cerr << "[x] Failed to create event handler." << std::endl;
        return -1;
    }

    BeaconScanner s;
    s.setEventHandler(evth);

    try {
        s.scan();
    } catch (const std::runtime_error & e) {
        std::cerr << "[x] " << e.what() << std::endl;
    }

    return 0;
}
