#include "ble_extensions.h"
#include "beacon_scanner.hpp"

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>

#include <stdint.h>
#include <unistd.h>
#include <signal.h>

#include <stdexcept>
#include <iostream>
#include <cstring>

bool BeaconScanner::sigterm_rcvd = false;
bool BeaconScanner::sigterm_reg = false;

void BeaconScanner::handle_SIGTERM(int signum)
{
    BeaconScanner::sigterm_rcvd = true;
}

BeaconScanner::BeaconScanner(const std::string device) : _scan_active(false)
{
    int dev_id = hci_devid(device.c_str());
    if (dev_id < 0) {
        throw std::runtime_error(
            "could not obtain device id for " + device + " (invalid device?)");
    }

    _dd = hci_open_dev(dev_id);
    if (_dd < 0) {
        throw std::runtime_error("could not open device " + device);
    }

    // Register SIGTERM handler once
    if (!BeaconScanner::sigterm_reg) {
        struct sigaction sa;
        sa.sa_handler = BeaconScanner::handle_SIGTERM;
        sa.sa_flags = SA_RESTART;
        sigemptyset(&sa.sa_mask);
        sigaction(SIGINT, &sa, NULL);
        BeaconScanner::sigterm_reg = true;
    }
}

void BeaconScanner::enableScanMode()
{
    uint8_t scan_type = 0x01; // active scan
	uint16_t interval = htobs(0x0010); // 10 ms
	uint16_t window = htobs(0x0010); // 10 ms
	uint8_t own_type = 0x00; // public device address
	uint8_t filter_policy = 0x00; // accept all

    // Set scan parameters with `timeout` 10000 ms = 10 s
    int r = hci_le_set_scan_parameters(
        _dd, scan_type, interval, window, own_type, filter_policy, 10000
    );
    if (r < 0)
        throw std::runtime_error("set scan parameters failed (are you root?)");

    // Enable scan with duplicate filtering enabled (timeout 10s)
    r = hci_le_set_scan_enable(_dd, 0x01, 0x01, 10000);
    if (r < 0)
        throw std::runtime_error("enable scan failed");

    _scan_active = true;
}

void BeaconScanner::disableScanMode()
{
    int r = hci_le_set_scan_enable(_dd, 0x00, 0x01, 10000);
    if (r < 0)
        throw std::runtime_error("disable scan failed");
    _scan_active = false;
}

void BeaconScanner::collectLEMetaEvents()
{
    // Backup old socket filter options
    struct hci_filter old_opts;
    socklen_t slen = sizeof(old_opts);
    if (getsockopt(_dd, SOL_HCI, HCI_FILTER, &old_opts, &slen) < 0)
        throw std::runtime_error("could not get socket options");

    // Set new socket filter options (will only receive LE meta events)
    struct hci_filter new_opts;
    hci_filter_clear(&new_opts);
    hci_filter_set_ptype(HCI_EVENT_PKT, &new_opts);
    hci_filter_set_event(EVT_LE_META_EVENT, &new_opts);
    if (setsockopt(_dd, SOL_HCI, HCI_FILTER, &new_opts, sizeof(new_opts)) < 0)
        throw std::runtime_error("could not set socket options");

    int len;
    uint8_t buf[HCI_MAX_EVENT_SIZE];

    fd_set readfd;
    struct timeval timeout;
    memset(&timeout, 0, sizeof(timeout));

    // Event capture loop
    while (!sigterm_rcvd) {
        FD_ZERO(&readfd);
        FD_SET(_dd, &readfd);
        timeout.tv_sec = 5;
        int r = select(_dd+1, &readfd, NULL, NULL, &timeout);
        if (r < 0) {
            if (errno == EAGAIN || errno == EINTR) continue;
            perror(NULL);
            break;
        } else if (r > 0) {
            len = read(_dd, buf, sizeof(buf));
            if (len < 0) {
                if (errno == EAGAIN || errno == EINTR) continue;
                perror(NULL);
                break;
            } else {
                processLEMetaEvent(buf, len);
            }
        }
        _bmgr.update();
    }

    std::cout << "[!] KILLED" << std::endl;

    // Restore old socket parameters
    setsockopt(_dd, SOL_HCI, HCI_FILTER, &old_opts, sizeof(old_opts));
}

void BeaconScanner::processLEMetaEvent(unsigned char * buf, size_t len)
{
    hci_event_hdr * hdr = (hci_event_hdr *) (buf + 1); // Event header
    unsigned char * bdy = buf + (1 + HCI_EVENT_HDR_SIZE); // Event body

    if (hdr->evt != EVT_LE_META_EVENT) // Type of event
        throw std::runtime_error("received unknown HCI event!");

    evt_le_meta_event * me = (evt_le_meta_event *) bdy; // Meta event body

    if (me->subevent != EVT_LE_ADVERTISING_REPORT) // Event subtype
        throw std::runtime_error("received unknown LE meta event!");

    le_advertising_report * rps = (le_advertising_report *) me;
    le_advertising_info * info;

    // For each reported advertisement
    for (int i = 0; i < rps->num_reports; ++i) {
        info = &rps->report[i];

        try { // Extract beacon information from advertising
            Beacon b = beaconFromAdvertisement(info->data, info->length);
            b.data.rssi = info->data[info->length];
            _bmgr.record(b);
        } catch (no_beacon_found& e) {
            continue;
        }
    }
}

Beacon BeaconScanner::beaconFromAdvertisement(uint8_t * buf, size_t len) {
    le_advertising_data * ad;
    // Iterate through the advertising response data. [Vol 3] Part C, Section 11
    for (size_t i = 0; i < len; i += ad->length+1) {
        ad = (le_advertising_data *) &buf[i];
        switch (ad->type) {
            // The beacon data is manufacturer specific data
            case AD_TYPE_MAN_SPEC:
                gen_beacon_data * bdata = (gen_beacon_data *) ad->data;
                // Test ibeacon signature and decode if ibeacon
                if (!std::memcmp(&bdata->sig[0], ibeacon_sig, 2)) {
                    return beaconFromiBeacon(ad->data, ad->length);
                }
                // Test altbeacon signature and decode if altbeacon
                if (!std::memcmp(&bdata->sig[0], altbeacon_sig, 2)) {
                    return beaconFromAltBeacon(ad->data, ad->length);
                }
                break;
        }
    }
    throw no_beacon_found();
}

Beacon BeaconScanner::beaconFromiBeacon(uint8_t * buf, size_t len) {
    ibeacon_data * data = (ibeacon_data *) buf;

    // Construct UUID string
    char str_uuid[37];
    sprintf(str_uuid, "%02X%02X%02X%02X-%02X%02X-%02X%02X-%02X%02X"
        "-%02X%02X%02X%02X%02X%02X",
        data->uuid[0], data->uuid[1], data->uuid[2], data->uuid[3],
        data->uuid[4], data->uuid[5], data->uuid[6], data->uuid[7],
        data->uuid[8], data->uuid[9], data->uuid[10], data->uuid[11],
        data->uuid[12], data->uuid[13], data->uuid[14], data->uuid[15]
    );

    // Construct beacon identifier
    Beacon::ID id{str_uuid, bswap_16(data->major), bswap_16(data->minor)};

    Beacon::Data bd;
    bd.tx = data->tx;

    return Beacon{id, bd};
}

Beacon BeaconScanner::beaconFromAltBeacon(uint8_t * buf, size_t len) {
    altbeacon_data * data = (altbeacon_data *) buf;

    // Construct UUID string
    char str_uuid[37];
    sprintf(str_uuid, "%02X%02X%02X%02X-%02X%02X-%02X%02X-%02X%02X"
        "-%02X%02X%02X%02X%02X%02X",
        data->bid[0], data->bid[1], data->bid[2], data->bid[3],
        data->bid[4], data->bid[5], data->bid[6], data->bid[7],
        data->bid[8], data->bid[9], data->bid[10], data->bid[11],
        data->bid[12], data->bid[13], data->bid[14], data->bid[15]
    );

    // Construct beacon identifier
    Beacon::ID id{
        str_uuid,
        bswap_16(*((uint16_t *)&data->bid[16])),
        bswap_16(*((uint16_t *)&data->bid[18]))
    };

    Beacon::Data bd;
    bd.tx = data->tx;

    return Beacon{id, bd};
}

void BeaconScanner::scan()
{
    enableScanMode();
    collectLEMetaEvents();
    disableScanMode();
}

BeaconScanner::~BeaconScanner()
{
    if (_scan_active) disableScanMode();
    int r = hci_close_dev(_dd);
    if (r < 0)
        perror(NULL);
}
