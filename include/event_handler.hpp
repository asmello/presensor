#ifndef EVENT_HANDLER_HPP
#define EVENT_HANDLER_HPP

#include "beacon.hpp"
#include "curl.hpp"

#include <json/json.h>

#include <string>

class EventHandler {
public:
    EventHandler(const std::string & hostname = "", short unsigned port = 80);
    void setRemote(const std::string & url) { _base_url = url; }

    void notifyDetection(Beacon b);
    void notifyLoss(Beacon::ID id);
    void flush();

private:
    Json::Value _events;
    std::string _base_url;
    Curl _cinst;

    Json::Value queryCredentials();
};

#endif
