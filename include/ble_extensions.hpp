#ifndef BLE_EXTENSIONS_HPP
#define BLE_EXTENSIONS_HPP

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>

// See https://www.bluetooth.org/en-us ...
// ... /specification/assigned-numbers/generic-access-profile
#define AD_TYPE_FLAGS 0x01
#define AD_TYPE_MAN_SPEC 0xFF

typedef struct {
    uint8_t subevent;
    uint8_t num_reports;
    le_advertising_info report[];
} __attribute__ ((packed)) le_advertising_report;

typedef struct {
    uint8_t length;
    uint8_t type;
    uint8_t data[];
} __attribute__ ((packed)) le_advertising_data;

typedef struct {
    uint8_t company_id[2];
    uint8_t sig[2]; // type signature
    uint8_t data[];
} __attribute__ ((packed)) gen_beacon_data;

typedef struct {
    uint8_t company_id[2];
    uint8_t sig[2]; // type signature
    uint8_t uuid[16];
    uint16_t major;
    uint16_t minor;
    int8_t tx;
} __attribute__ ((packed)) ibeacon_data;

typedef struct {
    uint8_t company_id[2];
    uint8_t sig[2]; // type signature
    uint8_t bid[20];
    int8_t tx;
    uint8_t rsvd; // manufacturer specific
} __attribute__ ((packed)) altbeacon_data;

const char ibeacon_sig[] = "\x02\x15";
const char altbeacon_sig[] = "\xAC\xBE";

#endif
