#ifndef BEACON_HPP
#define BEACON_HPP

#include <string>
#include <chrono>

struct Beacon {
    struct ID {
        std::string uuid;
        uint16_t major, minor;
        bool operator<(const ID & other) const
        {
            if (uuid == other.uuid) {
                if (major == other.major) {
                    return minor < other.minor;
                } else {
                    return major < other.major;
                }
            } else {
                return uuid < other.uuid;
            }
        }
    } id;
    struct Data {
        int8_t tx, rssi;
        bool first_detection;
        std::chrono::system_clock::time_point last_time;
        double distance();
    } data;
};




#endif
