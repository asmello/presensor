#ifndef BEACON_SCANNER_HPP
#define BEACON_SCANNER_HPP

#include <string>
#include <memory>

#include "beacon.hpp"
#include "event_handler.hpp"
#include "beacon_manager.hpp"

class BeaconScanner {
public:
    BeaconScanner(const std::string dev="hci0");
    ~BeaconScanner();
    void setEventHandler(std::shared_ptr<EventHandler> evth) {
        _bmgr.setEventHandler(evth);
    }
    void scan();

private:
    static bool sigterm_reg;
    static bool sigterm_rcvd;

    struct no_beacon_found : public std::runtime_error {
        no_beacon_found() : std::runtime_error(
                "no beacon could be parsed from the advertisement") {}
    };

    int _dd;
    bool _scan_active;
    BeaconManager _bmgr;

    void enableScanMode();
    void disableScanMode();
    void collectLEMetaEvents();
    void processLEMetaEvent(uint8_t * buf, size_t len);
    Beacon beaconFromAdvertisement(uint8_t * buf, size_t len);
    Beacon beaconFromiBeacon(uint8_t * buf, size_t len);
    Beacon beaconFromAltBeacon(uint8_t * buf, size_t len);
    static void handle_SIGTERM(int signum);
};

#endif
