#ifndef CURL_HPP
#define CURL_HPP

#include <curl/curl.h>

#include <stdexcept>
#include <iostream>
#include <sstream>
#include <string>

class Curl {
public:
    Curl();
    ~Curl();

    void reset();

    void get(const std::string& addr);

    void post(const std::string& addr,
              const std::string& mime,
              const std::string& msg);

    void post(const std::string& addr,
              const std::string& mime,
              const std::string& msg,
              const size_t timeout);

    long getResponseCode();
    void setCredentials(const std::string& usr, const std::string& pwd);

    operator std::istream&() { return static_cast<std::istream&>(_rcvss); }
    operator std::ostream&() { return static_cast<std::ostream&>(_sndss); }

    friend std::ostream& operator<<(std::ostream& output, const Curl& o);
    friend std::istream& operator>>(std::istream& input, Curl& o);

    struct curl_error : public std::runtime_error {
        curl_error(const char *str) : std::runtime_error(str) {}
        curl_error(const std::string& str) : std::runtime_error(str) {}
    };

    static const long HTTP_OK = 200;
    static const long HTTP_BAD_REQUEST = 400;
    static const long HTTP_UNAUTHORIZED = 401;
    static const long HTTP_NOT_FOUND = 404;

private:
    static int num_instances;
    static const char * user_agent;
    static size_t write_data(void *buf, size_t size, size_t nmemb, void *userp);
    static size_t read_data(void *buf, size_t size, size_t nitems, void *userp);

    CURL *_handle;
    std::stringstream _sndss;
    std::stringstream _rcvss;
};

#endif
