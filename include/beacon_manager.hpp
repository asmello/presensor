#ifndef BEACON_MANAGER_HPP
#define BEACON_MANAGER_HPP

#include "beacon.hpp"
#include "event_handler.hpp"

#include <map>
#include <memory>

class BeaconManager {
public:
    void setEventHandler(std::shared_ptr<EventHandler> evth) { _evth = evth; }
    void record(Beacon b);
    void update();

private:
    std::map<Beacon::ID, Beacon::Data> _record;
    std::shared_ptr<EventHandler> _evth;
};

#endif
