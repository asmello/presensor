PROJNAME= bscan
CC		= g++
EXTERN  = jsoncpp cpp-argparse
SRCEXT	= cpp
SRCDIR	= src
INCDIR	= include
OBJDIR	= obj
BINDIR	= bin
# remover -O3 -Wno-unused-result -Wno-unused-parameter em modo debug
CFLAGS	= -std=c++14
LFLAGS	= -lbluetooth `curl-config --libs`
WFLAGS	= -Wall -Wextra -Wuninitialized -pedantic
DFLAGS  = -DVERSION=\"0.1\"

# Utilizando a opcao DEBUG = 1, a quantidade de operacoes do algoritmo é
# printada ao termino do mesmo. OBS.: caso já tenha compilado com a opcao
# DEBUG = 0, execute primeiro 'make clean', mude o DEBUG para 1 e depois compile
DEBUG	= 1

#------------------------------------------------------------------------------

ifeq ($(DEBUG),1)
	DFLAGS += -DDEBUG
	debug = -g $(WFLAGS)
else
	debug =
endif

EXTERNAL = $(addprefix extern/,$(EXTERN))
INCDIR  += $(EXTERNAL)
SRCDIR  += $(EXTERNAL)

INCLUDES = $(addprefix -I,$(INCDIR))
CFLAGS	+= -c $(INCLUDES) $(debug)
SRCS	:= $(shell find $(SRCDIR) -name '*.$(SRCEXT)')
SRCDIRS := $(shell find . -name '*.$(SRCEXT)' -exec dirname {} \; | uniq)
OBJS    := $(patsubst %.$(SRCEXT),$(OBJDIR)/%.o,$(SRCS))

.PHONY: all clean distclean

all: $(BINDIR)/$(PROJNAME)

$(BINDIR)/$(PROJNAME): buildrepo $(OBJS)
	@mkdir -p `dirname $@`
	@echo "Linking $@..."
	@$(CC) $(OBJS) $(LFLAGS) -o $@

$(OBJDIR)/%.o: %.$(SRCEXT)
	@echo "Generating dependencies for $<..."
	@$(call make-depend,$<,$@,$(subst .o,.d,$@))
	@echo "Compiling $<..."
	@$(CC) $(CFLAGS) $(DFLAGS) $< -o $@

clean:
	$(RM) -r $(OBJDIR)

distclean: clean
	$(RM) -r $(BINDIR)

buildrepo:
	@$(call make-repo)

define make-repo
   for dir in $(SRCDIRS); \
   do \
	mkdir -p $(OBJDIR)/$$dir; \
   done
endef

# usage: $(call make-depend,source-file,object-file,depend-file)
define make-depend
  $(CC) -MM       \
        -MF $3    \
        -MP       \
        -MT $2    \
        $(CFLAGS) \
        $1
endef
